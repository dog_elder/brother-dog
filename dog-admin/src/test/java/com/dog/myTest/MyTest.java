package com.dog.myTest;


import com.dog.common.utils.JedisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @program: dog
 * @description:
 * @author: @Dog_Elder
 * @create: 2021-03-13 11:59
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MyTest extends Thread{



    /**
     * @Description: 测试Mybatis添加
     * @Author: @Dog_Elder
     * @Date: 2021-3-25 15:16
     * @return: void
     **/
    @Test
    public void a1() {
//        ZygxyClassGrade grade = new ZygxyClassGrade();
        PageHelper.startPage(1,3);
//        classGradeService.save(ZygxyClassGrade.builder().className("2021年3月24日 14:06:34").departmentId(1l).periodId(1l).build());

    }


    /**
     * @Description: 测试MyBatis修改
     * @Author: @Dog_Elder
     * @Date: 2021-3-25 15:17
     * @return: void
     **/
    @Test
    public void a2() {
//        ZygxyClassGrade grade = new ZygxyClassGrade();

//        classGradeService.updateById(ZygxyClassGrade.builder().classId(16l).className("修改2").departmentId(1l).periodId(1l).build());
//        classGradeService.save(ZygxyClassGrade.builder().className("2021年3月24日 14:06:34").departmentId(1l).periodId(1l).build());
//        List<ZygxyClassGrade> list =
//                classGradeService.list();
//        for (ZygxyClassGrade zygxyClassGrade : list) {
//            System.out.println("zygxyClassGrade = " + zygxyClassGrade.getCreateBy());
//            System.out.println("zygxyClassGrade = " + zygxyClassGrade.getCreateTime());
//        }
//        System.out.println("list.size() = " + list.size());
    }

    /**
     * @Description: 测试
     * @Author: @Dog_Elder
     * @Date: 2021-3-25 20:17
     * @return: void
     **/
    @Test
    public void a3() {
        String json = JedisUtil.getJson("1");
        System.out.println("json = " + json);

    }

    @Test
    public void a4() {
         }




}

