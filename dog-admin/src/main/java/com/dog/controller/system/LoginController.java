package com.dog.controller.system;

import com.dog.common.core.domain.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @program: dog
 * @description: 后台管理登录
 * @author: @Dog_Elder
 * @create: 2021-03-05 19:43
 **/
@Controller
@RequestMapping()
public class LoginController {

    @GetMapping
    public String login(){
        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public R login(@NotNull(message = "用户名不能为空") @NotBlank(message = "用户名不能为空") String username, @NotBlank(message = "密码不能为空") String password){
        System.out.println("登录账号" + username);
        System.out.println("登录人密码" + password);
        return R.success();
    }
}
