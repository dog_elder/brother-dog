package com.dog;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author @Dog_Elder
 */
@Slf4j
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan(value = {"com.dog.mapper","com.dog.*.mapper"})
public class DogApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DogApplication.class, args);
        log.info("神奇的事情发生了!");

    }
}
