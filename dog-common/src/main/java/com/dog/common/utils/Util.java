package com.dog.common.utils;

/**
 * @program: dog
 * @description: 工具类
 * @author: @Dog_Elder
 * @create: 2021-06-02 15:20
 **/
public class Util {

    /**
     * * 判断一个对象是否为空
     *
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * * 判断一个对象是否非空
     *
     * @param object Object
     * @return true：非空 false：空
     */
    public static boolean isNotNull(Object object) {
        return !Util.isNull(object);
    }
}
