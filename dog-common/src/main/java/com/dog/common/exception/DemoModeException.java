package com.dog.common.exception;

/**
 * 演示模式异常
 * 
 * @author @Dog_Elder
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
