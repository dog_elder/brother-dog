package com.dog.common.constant;


/**
 * @Description:常量
 * @Author: @Dog_Elder
 * @Date: 2018/9/3 16:03
 **/
public class Constant {


    /**
     * @Description: redis 时间
     * @Author: @Dog_E
     * @Date: 2020/7/15 0015
     */
    public interface RedisTimesConstant {
        /**
         * redis过期时间，以秒为单位，一分钟
         */
        public final static int EXRP_MINUTE = 60;

        /**
         * key过渡时间，以秒为单位,10秒
         */
        public final static int EXRP_SECOND = 20;

        /**
         * redis过期时间，以秒为单位，一小时
         */
        public final static int EXRP_HOUR = 60 * 60;

        /**
         * redis过期时间，以秒为单位，一天
         */
        public final static int EXRP_DAY = 60 * 60 * 24;
    }

    /**
     * @Description: redis前缀
     * @Author: @Dog_E
     * @Date: 2020/7/15 0015
     */
    public interface RedisPrefixConstant {
        /**
         * redis-OK
         */
        public final static String OK = "OK";

    }


    /**
     * @Description: 机构
     * @Author: @Dog_Elder
     * @Date: 2020/12/3 9:00
     **/
    public interface Institutional {
        /**
         * 机构统计
         */
        public static final String INSTITUTIONAL_STATISTICS = "institutional:statistics";
    }


    /**
     * LOCK_SUCCESS 锁成功
     */
    public static final String LOCK_SUCCESS = "OK";
    public static final String SET_IF_NOT_EXIST = "NX";
    public static final String SET_WITH_EXPIRE_TIME = "PX";
    public static final Long RELEASE_SUCCESS = 1L;


}
